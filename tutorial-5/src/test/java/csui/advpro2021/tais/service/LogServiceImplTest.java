package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;

    @InjectMocks
    private LogServiceImpl logService;

    private Log log;

    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa();
        mahasiswa.setNama("Anski Capek");
        mahasiswa.setNpm("1906010101");
        mahasiswa.setEmail("anskihhh@cs.ui.ac.id");
        mahasiswa.setIpk("3.75");
        mahasiswa.setNoTelp("0815010101");

        Calendar calendar = Calendar.getInstance();
        calendar.set(2021, 1, 1, 0, 0, 0);
        Date start = calendar.getTime();
        calendar.set(2021, 1, 1, 4, 0, 0);
        Date end = calendar.getTime();

        log = new Log();
        log.setIdLog(1);
        log.setStartTime(start);
        log.setEndTime(end);
        log.setDeskripsi("Hnggnngngngn");
        log.setMahasiswa(mahasiswa);
    }

    @Test
    public void testServiceCreateLog() {
        lenient().when(logService.createLog(log)).thenReturn(log);
        lenient().when(logService.createLog(mahasiswa, log)).thenReturn(log);
    }

    @Test
    public void testLogServiceGetListLog() {
        Iterable<Log> listLog = logRepository.findAll();
        lenient().when(logService.getListLog()).thenReturn(listLog);
        Iterable<Log> listLogResult = logService.getListLog();
        assertIterableEquals(listLog, listLogResult);
    }

    @Test
    public void testServiceGetLogByIdLog() {
        lenient().when(logService.getLogByIdLog(1)).thenReturn(log);
        Log resultLog = logService.getLogByIdLog(log.getIdLog());
        assertEquals(log.getIdLog(), resultLog.getIdLog());
    }

    @Test
    public void testLogDeleteLog() {
        logService.createLog(mahasiswa, log);
        logService.deleteLogByIdLog(log.getIdLog());
        lenient().when(logService.getLogByIdLog(log.getIdLog())).thenReturn(null);
        assertEquals(null, logService.getLogByIdLog(log.getIdLog()));
    }

    @Test
    public void testServiceUpdateLog() {
        logService.createLog(mahasiswa, log);
        String currentDesc = log.getDeskripsi();
        log.setDeskripsi("Hngngnngngn!!!");

        lenient().when(logService.updateLog(log.getIdLog(), log)).thenReturn(log);
        Log resultLog = logService.updateLog(log.getIdLog(), log);

        assertNotEquals(resultLog.getDeskripsi(), currentDesc);
        assertEquals(resultLog.getEndTime(), log.getEndTime());
    }

    @Test void testServiceUpdateLogMahasiswa() {
        logService.createLog(mahasiswa, log);
        Mahasiswa newMahasiswa = new Mahasiswa("0", "0", "0", "0", "0");
        Mahasiswa currentMahasiswa = log.getMahasiswa();
        log.setMahasiswa(newMahasiswa);

        lenient().when(logService.updateLog(log.getIdLog(), newMahasiswa, log)).thenReturn(log);
        Log resultLog = logService.updateLog(log.getIdLog(), newMahasiswa, log);

        assertNotEquals(resultLog.getMahasiswa(), currentMahasiswa);
        assertEquals(resultLog.getEndTime(), log.getEndTime());
    }

    @Test
    public void testServiceGetSummary() {
        List<Log> logList = new ArrayList<>();
        logList.add(log);
        lenient().when(logRepository.findAllByMahasiswa(mahasiswa)).thenReturn(logList);

        List<LogSummary> expectedLogList = new ArrayList<>();
        expectedLogList.add(new LogSummary("February", 4, 1400L));

        Collection<LogSummary> logSummaries = logService.getSummary(mahasiswa);
        assertIterableEquals(expectedLogList, logSummaries);
    }

    @Test
    public void testServiceGetSummaryMonth() {
        List<Log> logList = new ArrayList<>();
        logList.add(log);
        lenient().when(logRepository.findAllByMahasiswa(mahasiswa)).thenReturn(logList);

        LogSummary expectedSummary = new LogSummary("February", 4, 1400L);

        LogSummary logSummaries = logService.getSummaryMonth(mahasiswa, 1);
        assertEquals(expectedSummary, logSummaries);
    }
}
