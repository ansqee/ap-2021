package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LogController.class)
public class LogControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    @MockBean
    private MataKuliahServiceImpl mataKuliahService;

    private Log log;

    private Mahasiswa mahasiswa;

    private MataKuliah mataKuliah;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa("1906010101", "Anski Anskrt", "anski@ui.ac.id", "3.5", "0813010101");

        Calendar calendar = Calendar.getInstance();
        calendar.set(2021, 1, 1, 0, 0, 0);
        Date start = calendar.getTime();
        calendar.set(2021, 1, 1, 4, 0, 0);
        Date end = calendar.getTime();

        log = new Log(start, end, "Brbrbbrr");
        log.setIdLog(1);
        log.setMahasiswa(mahasiswa);

        mataKuliah = new MataKuliah("AP", "AdPig", "Ilkom");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    // test untuk bikin post
    @Test
    public void testControllerPostNewLog() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(mataKuliahService.getMataKuliah(mataKuliah.getKodeMatkul())).thenReturn(mataKuliah);
        when(mahasiswaService.setAsdos(mahasiswa, mataKuliah)).thenReturn(mahasiswa);
        when(logService.createLog(any(Mahasiswa.class), any(Log.class))).thenReturn(log);
        mahasiswa.setMatkulAsdos(mataKuliah);

        String logStr = "{\"startTime\":949334400325,\"endTime\":949348800325,\"deskripsi\":\"AAAA\"}";

        mvc.perform(post("/log/" + mahasiswa.getNpm() + "/log")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(logStr))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deskripsi").value(log.getDeskripsi()));
    }

    @Test
    public void testControllerPostNewLogMahasiswaNotFound() throws Exception {
        String logStr = "{\"startTime\":949334400325,\"endTime\":949348800325,\"deskripsi\":\"AAAA\"}";
        mvc.perform(post("/log/" + mahasiswa.getNpm() + "/log")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(logStr))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerPostNewLogNoMatkulAsdos() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(mataKuliahService.getMataKuliah(mataKuliah.getKodeMatkul())).thenReturn(mataKuliah);
        String logStr = "{\"startTime\":949334400325,\"endTime\":949348800325,\"deskripsi\":\"AAAA\"}";
        mvc.perform(post("/log/" + mahasiswa.getNpm() + "/log")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(logStr))
                .andExpect(status().isNotImplemented());
    }

    // untuk update
    @Test
    public void testControllerUpdateLog() throws Exception {
        when(logService.getLogByIdLog(log.getIdLog())).thenReturn(log);

        Calendar calendar = Calendar.getInstance();
        calendar.set(2000, 1, 1, 0, 0, 0);
        Date start = calendar.getTime();
        calendar.set(2000, 1, 1, 4, 0, 0);
        Date end = calendar.getTime();

        Log logUdahAda = new Log(start, end, "AAAA");
        String logStr = "{\"startTime\":949334400325,\"endTime\":949348800325,\"deskripsi\":\"AAAA\"}";
        when(logService.updateLog(anyInt(), any(Log.class))).thenReturn(logUdahAda);

        mvc.perform(put("/log/" + log.getIdLog())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(logStr))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deskripsi").value("AAAA"));
    }

    @Test
    public void testControllerUpdateLogNull() throws Exception {
        String logStr = "{\"startTime\":949334400325,\"endTime\":949348800325,\"deskripsi\":\"AAAA\"}";
        mvc.perform(put("/log/" + log.getIdLog())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(logStr))
                .andExpect(status().isNotFound());
    }

    // untuk delete log
    @Test
    public void testControllerDeleteLog() throws Exception {
        when(logService.getLogByIdLog(log.getIdLog())).thenReturn(log);

        mvc.perform(delete("/log/" + log.getIdLog()))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testControllerDeleteLogNull() throws Exception {
        mvc.perform(delete("/log/" + log.getIdLog()))
                .andExpect(status().isNotFound());
    }

    // untuk get log
    @Test
    public void testControllerGetLog() throws Exception {
        mahasiswa.setMatkulAsdos(mataKuliah);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        List<Log> resultLogs = new ArrayList<>();
        resultLogs.add(log);
        when(logService.getLogsMahasiswa(mahasiswa)).thenReturn(resultLogs);

        mvc.perform(get("/log/" + mahasiswa.getNpm()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].deskripsi").value("Brbrbbrr"));
    }

    @Test
    public void testControllerGetLogNoMahasiswa() throws Exception {
        mvc.perform(get("/log/" + mahasiswa.getNpm()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerGetLogNoMatkulAsdos() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        mvc.perform(get("/log/" + mahasiswa.getNpm()))
                .andExpect(status().isNotImplemented());
    }

    // untuk get summary
    @Test
    public void testControllerGetSummary() throws Exception {
        mahasiswa.setMatkulAsdos(mataKuliah);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);

        List<LogSummary> logSummaries = new ArrayList<>();
        logSummaries.add(new LogSummary("1", 1, 1L));
        when(logService.getSummary(mahasiswa)).thenReturn(logSummaries);

        mvc.perform(get("/log/" + mahasiswa.getNpm() + "/log-report"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].month").value("1"));
    }

    @Test
    public void testControllerGetSummaryNoMahasiswa() throws Exception {
        mvc.perform(get("/log/" + mahasiswa.getNpm() + "/log-report"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerGetSummaryNoMatkulAsdos() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        mvc.perform(get("/log/" + mahasiswa.getNpm() + "/log-report"))
                .andExpect(status().isNotImplemented());
    }

    // untuk get summary month
    @Test
    public void testControllerGetSummaryMonth() throws Exception {
        mahasiswa.setMatkulAsdos(mataKuliah);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        LogSummary logSummary = new LogSummary("1", 1, 1L);
        when(logService.getSummaryMonth(mahasiswa, 1)).thenReturn(logSummary);

        mvc.perform(get("/log/" + mahasiswa.getNpm() + "/log-report/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.month").value("1"));
    }

    @Test
    public void testControllerGetSummaryMonthNoMahasiswa() throws Exception {
        mvc.perform(get("/log/" + mahasiswa.getNpm() + "/log-report/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerGetSummaryMonthNoMatkulAsdos() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        mvc.perform(get("/log/" + mahasiswa.getNpm() + "/log-report/1"))
                .andExpect(status().isNotImplemented());
    }
}
