package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;

import java.util.Collection;
import java.util.List;

public interface LogService {
    Log createLog(Log log);

    Log createLog(Mahasiswa mahasiswa, Log log);

    Iterable<Log> getListLog();

    Log getLogByIdLog(Integer idLog);

    void deleteLogByIdLog(Integer idLog);

    List<Log> getLogsMahasiswa(Mahasiswa mahasiswa);

    Collection<LogSummary> getSummary(Mahasiswa mahasiswa);

    LogSummary getSummaryMonth(Mahasiswa mahasiswa, int month);

    Log updateLog(Integer idLog, Log log);

    Log updateLog(Integer idLog, Mahasiswa mahasiswa, Log log);
}
