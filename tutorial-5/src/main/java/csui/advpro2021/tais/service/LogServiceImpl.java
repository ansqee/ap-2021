package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class LogServiceImpl implements LogService{
    @Autowired
    private LogRepository logRepository;

    @Override
    public Log createLog(Log log) {
        logRepository.save(log);
        return log;
    }

    @Override
    public Log createLog(Mahasiswa mahasiswa, Log log) {
        log.setMahasiswa(mahasiswa);
        return createLog(log);
    }

    @Override
    public Iterable<Log> getListLog() {
        return logRepository.findAll();
    }

    @Override
    public Log getLogByIdLog(Integer idLog) {
        return logRepository.findByIdLog(idLog);
    }

    @Override
    public void deleteLogByIdLog(Integer idLog) {
        logRepository.deleteById(idLog);
    }

    @Override
    public List<Log> getLogsMahasiswa(Mahasiswa mahasiswa) {
        return logRepository.findAllByMahasiswa(mahasiswa);
    }

    @Override
    public Collection<LogSummary> getSummary(Mahasiswa mahasiswa) {
        Calendar calendar = Calendar.getInstance();
        List<Log> logs = getLogsMahasiswa(mahasiswa);
        TreeMap<Integer, LogSummary> result = new TreeMap<>();

        for(Log log : logs) {
            Date startTime = log.getStartTime();
            Date endDate = log.getEndTime();
            int totalTime = (int) ((endDate.getTime() - startTime.getTime()) / (60 * 60 * 1000));

            calendar.setTime(startTime);
            int monthNumber = calendar.get(Calendar.MONTH);
            String month = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());

            LogSummary logSummary = result.getOrDefault(monthNumber, new LogSummary(month));
            logSummary.setJamKerja(logSummary.getJamKerja() + totalTime);
            logSummary.setPembayaran(logSummary.getJamKerja() * 350L);
            result.put(monthNumber, logSummary);
        }
        return result.values();
    }

    @Override
    public LogSummary getSummaryMonth(Mahasiswa mahasiswa, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(0, month, 1);
        List<Log> logs = getLogsMahasiswa(mahasiswa);
        String monthString = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        LogSummary logSummary = new LogSummary(monthString);

        for(Log log : logs) {
            calendar.setTime(log.getStartTime());
            if (calendar.get(Calendar.MONTH) == month) {
                Date start = log.getStartTime();
                Date end = log.getEndTime();
                int diff = (int) ((end.getTime() - start.getTime()) / (60 * 60 * 1000));

                logSummary.setJamKerja(logSummary.getJamKerja() + diff);
            }
        }
        logSummary.setPembayaran(logSummary.getJamKerja() * 350L);
        return logSummary;
    }

    @Override
    public Log updateLog(Integer idLog, Log log) {
        log.setIdLog(idLog);
        logRepository.save(log);
        return log;
    }

    @Override
    public Log updateLog(Integer idLog, Mahasiswa mahasiswa, Log log) {
        log.setMahasiswa(mahasiswa);
        return updateLog(idLog, log);
    }

}
