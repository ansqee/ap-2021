package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LogService;
import csui.advpro2021.tais.service.MahasiswaService;
import csui.advpro2021.tais.service.MataKuliahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/log")
public class LogController {
    @Autowired
    private LogService logService;

    @Autowired
    private MahasiswaService mahasiswaService;

    @Autowired
    private MataKuliahService mataKuliahService;

    // untuk menunjukkan log dengan npm
    @GetMapping(path = "/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLog(@PathVariable(value = "npm") String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null) return new ResponseEntity(HttpStatus.NOT_FOUND);
        if (mahasiswa.getMatkulAsdos() == null) return new ResponseEntity(HttpStatus.NOT_IMPLEMENTED);
        return ResponseEntity.ok(logService.getLogsMahasiswa(mahasiswa));
    }

    // untuk membuat log
    @PostMapping(path = "/{npm}/log", produces = {"application/json"}, consumes = {"application/json"})
    @ResponseBody
    public ResponseEntity postLog(@PathVariable(value = "npm") String npm, @RequestBody Log log) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null) return new ResponseEntity(HttpStatus.NOT_FOUND);
        if (mahasiswa.getMatkulAsdos() == null) return new ResponseEntity(HttpStatus.NOT_IMPLEMENTED);
        return ResponseEntity.ok(logService.createLog(mahasiswa, log));
    }

    // untuk mengupdate log
    @PutMapping(path = "/{idLog}", produces = {"application/json"})
    public ResponseEntity updateLog(@PathVariable(value = "idLog") Integer idLog, @RequestBody Log log) {
        Log lognya = logService.getLogByIdLog(idLog);
        if (lognya == null) return new ResponseEntity(HttpStatus.NOT_FOUND);
        return ResponseEntity.ok(logService.updateLog(idLog, log));
    }

    // untuk mendelete log
    @DeleteMapping(path = "/{idLog}", produces = {"application/json"})
    public ResponseEntity deleteLog(@PathVariable(value = "idLog") Integer idLog) {
        Log log = logService.getLogByIdLog(idLog);
        if (log == null) return new ResponseEntity(HttpStatus.NOT_FOUND);
        logService.deleteLogByIdLog(idLog);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    // untuk melihat log report
    @GetMapping(path = "/{npm}/log-report", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<LogSummary>> getSummary(@PathVariable(value = "npm") String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null) return new ResponseEntity(HttpStatus.NOT_FOUND);
        if (mahasiswa.getMatkulAsdos() == null) return new ResponseEntity(HttpStatus.NOT_IMPLEMENTED);
        return ResponseEntity.ok(logService.getSummary(mahasiswa));
    }

    // untuk melihat log report monthly
    @GetMapping(path = "/{npm}/log-report/{month}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<LogSummary> getSummaryMonth(@PathVariable(value = "npm") String npm, @PathVariable(value = "month") Integer month) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null) return new ResponseEntity(HttpStatus.NOT_FOUND);
        if (mahasiswa.getMatkulAsdos() == null) return new ResponseEntity(HttpStatus.NOT_IMPLEMENTED);
        return ResponseEntity.ok(logService.getSummaryMonth(mahasiswa, month));
    }
}
