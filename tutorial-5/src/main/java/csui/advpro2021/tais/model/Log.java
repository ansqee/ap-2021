package csui.advpro2021.tais.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "log")
@Data
@NoArgsConstructor
public class Log {
    @Id
    @Column(name = "id_log", updatable = false, nullable = false)
    private Integer idLog;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_time", nullable = false)
    private Date startTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_time", nullable = false)
    private Date endTime;

    @Column(name = "deskripsi")
    private String deskripsi;

    @ManyToOne
    @JoinColumn(name = "mahasiswa", nullable = false)
    private Mahasiswa mahasiswa;

    public Log(Date startTime, Date endTime, String deskripsi) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.deskripsi = deskripsi;
    }
}
