package csui.advpro2021.tais.model;

import lombok.Data;

@Data
public class LogSummary {
    private String month;
    private Integer jamKerja;
    private Long pembayaran;

    public LogSummary(String month, Integer jamKerja, Long pembayaran) {
        this.month = month;
        this.jamKerja = jamKerja;
        this.pembayaran = pembayaran;
    }

    public LogSummary(String month) {
        this(month, 0, 0L);
    }
}
