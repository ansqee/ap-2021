# Requirements

## Models Persistence

- **MataKuliah**<br>
KodeMatkul: String (Primary Key)<br>
namaMatkul: String<br>
prodi: String<br>

- **Mahasiswa**<br>
NPM: String (Primary Key)<br>
Nama : String<br>
Email: String<br>
ipk: String<br>
noTelp: String<br>
matkulAsdos: ForeignKey MataKuliah<br>

- **Log**<br>
idLog: integer (Primary Key)<br>
startTime: datetime<br>
endTime: datetime<br>
deskripsi: Text<br>
mahasiswa: FK Mahasiswa<br>

## Log

- Dapat dibuat log dengan start time, end time, dan deskripsi
- Log bisa dihapus dan diupdate
- Terdapat fitur untuk melihat semua log seorang mahasiswa 
- Terdapat fitur log summary, bisa melihat log dan gaji yang didapatkan
- Terdapat fitur log summary month, bisa melihat log dan gaji perbulan
