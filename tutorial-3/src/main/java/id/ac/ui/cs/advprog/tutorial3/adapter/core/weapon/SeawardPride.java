package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class SeawardPride implements Weapon {


    private String holderName;

    public SeawardPride(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String normalAttack() {
        return holderName + " just did 6 consecutive spear strikes using " + getName();
    }

    @Override
    public String chargedAttack() {
        return holderName + " consumes stamina and lunged forward at a high speed using " + getName();
    }

    @Override
    public String getName() {
        return "Seaward Pride";
    }

    @Override
    public String getHolderName() { return holderName; }
}
