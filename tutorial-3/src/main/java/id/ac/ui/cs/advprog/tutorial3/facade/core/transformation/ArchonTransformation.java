package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;

/**
 * Kelas ini melakukan Caesar Chiper dengan shift amount sebesar 4
 **/
public class ArchonTransformation {
    private static int shiftAmount;

    public ArchonTransformation() {
        shiftAmount = 4;
    }

    public Spell encode(Spell spell) {
        return process(spell, true);
    }

    public Spell decode(Spell spell) {
        return process(spell, false);
    }

    private Spell process(Spell spell,  boolean encode) {
        int inc;
        if(encode) {
            inc = 1;
        } else {
            inc = -1;
        }
        Codex codex = spell.getCodex();
        int codexLen = codex.getCharSize();
        char[] res = spell.getText().toCharArray();
        for (int i = 0; i < res.length; i++) {
            int newI = codex.getIndex(res[i]) + (shiftAmount * inc);
            if(newI < 0) {
                newI += codexLen;
            } else {
                newI %= codexLen;
            }
            res[i] = codex.getChar(newI);
        }

        return new Spell(new String(res), codex);
    }
}
