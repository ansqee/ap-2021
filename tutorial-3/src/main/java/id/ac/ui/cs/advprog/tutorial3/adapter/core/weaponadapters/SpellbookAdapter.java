package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {
    private Spellbook spellbook;
    private boolean hasEnergy;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
    }

    @Override
    public String normalAttack() {
        hasEnergy = true;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if(hasEnergy) {
            hasEnergy = false;
            return spellbook.largeSpell();
        } else {
            hasEnergy = true;
            return "Elemental burst available, ready to be cast";
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
