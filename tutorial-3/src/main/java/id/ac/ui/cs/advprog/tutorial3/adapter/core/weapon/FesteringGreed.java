package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class FesteringGreed implements Weapon {

    private String holderName;

    public FesteringGreed(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String normalAttack() {
        return holderName + " just did 6 consecutive sword strikes using " + getName();
    }

    @Override
    public String chargedAttack() {
        return holderName + " consumes stamina and unleashed 2 strong rapid strikes using " + getName();
    }

    @Override
    public String getName() {
        return "Festering Greed";
    }

    @Override
    public String getHolderName() { return holderName; }
}
