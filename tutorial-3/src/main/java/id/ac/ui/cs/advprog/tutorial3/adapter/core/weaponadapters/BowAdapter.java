package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;

public class BowAdapter implements Weapon {
    private Bow bow;
    private boolean isAimed = false;

    public BowAdapter(Bow bow){
        this.bow = bow;
    }

    @Override
    public String normalAttack() {
        return bow.shootArrow(isAimed);
    }

    @Override
    public String chargedAttack() {
        if(isAimed) {
            isAimed = false;
            return "Switched to hip fire mode";
        } else  {
            isAimed = true;
            return "Locked on target, ready to fire!";
        }
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        return bow.getHolderName();
    }
}
