package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class FullMoonPike implements Weapon {

    private String holderName;

    public FullMoonPike(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String normalAttack() {
        return holderName + " just did 6 consecutive spear strikes using " + getName();
    }

    @Override
    public String chargedAttack() {
        return holderName + " consumes stamina and lunged forward at a high speed using " + getName();
    }

    @Override
    public String getName() {
        return "Full Moon Pike";
    }

    @Override
    public String getHolderName() { return holderName; }
}
