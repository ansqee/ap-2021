package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WeaponServiceImpl implements WeaponService {
    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    private boolean hasBeenMade = false;

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        if(!hasBeenMade) {
            for (Spellbook spellbook :
                    spellbookRepository.findAll()) {
                weaponRepository.save(new SpellbookAdapter(spellbook));
            }
            for (Bow bow :
                    bowRepository.findAll()) {
                weaponRepository.save(new BowAdapter(bow));
            }
            hasBeenMade = true;
            return weaponRepository.findAll();
        } else  {
            return weaponRepository.findAll();
        }

    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon currentWeapon = this.weaponRepository.findByAlias(weaponName);
        weaponRepository.save(currentWeapon);
        String attack;
        if(attackType == 0) {
            attack = currentWeapon.normalAttack();
        } else  {
            attack = currentWeapon.chargedAttack();
        }
        logRepository.addLog(attack);
    }

    @Override
    public List<String> getAllLogs() {
        return this.logRepository.findAll();
    }
}
