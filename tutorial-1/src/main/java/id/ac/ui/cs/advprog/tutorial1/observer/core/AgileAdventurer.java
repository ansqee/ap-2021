package id.ac.ui.cs.advprog.tutorial1.observer.core;
import java.util.List;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
        this.guild.add(this);
    }

    @Override
    public void update() {
        List<Quest> questList = this.getQuests();
        Quest quest = this.guild.getQuest();
        String questType = this.guild.getQuestType();
        if(questType.equals("D") || questType.equals("R")){
            questList.add(quest);
        }
    }

}
