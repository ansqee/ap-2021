package id.ac.ui.cs.advprog.tutorial1.observer.core;
import java.util.List;

public class KnightAdventurer extends Adventurer {


    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        this.guild = guild;
        this.guild.add(this);
    }

    @Override
    public void update() {
        List<Quest> questList = this.getQuests();
        Quest quest = this.guild.getQuest();
        questList.add(quest);
    }

}
